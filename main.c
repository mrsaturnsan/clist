#include "list.h"

#include <stdio.h>

init_list(int)

void print(int* i)
{
    printf("%d\n", *i);
}

bool comp(const int* a, const int* b)
{
    return *a >= *b;
}

int main(void)
{
    // allocate the list
    list(int) list = create_list(int);

    for (int i = 1; i <= 10; ++i)
        push_back_list(int, list, i);


    for (int i = 10; i > 1; --i)
        erase_element_custom_list(int, list, i, comp);

    foreach_list(int, list, print);

    // free allocated memory
    clear_list(int, list);

    return 0;
}