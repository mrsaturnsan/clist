CC = gcc
CFLAGS = -g -O3 -Wall -Wextra -Werror -pedantic
FILE = main.c
OUT = output

build:
	@$(CC) $(CFLAGS) $(FILE) -o $(OUT)

pre:
	@$(CC) -E $(FILE) -o main.i

run:
	@./$(OUT)

clean:
	@rm $(OUT)